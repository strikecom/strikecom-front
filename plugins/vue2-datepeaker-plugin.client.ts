import DatePicker from 'vue2-datepicker';
import Vue from 'vue';
import 'vue2-datepicker/index.css';
import 'vue2-datepicker/locale/ru';

// @ts-ignore
Vue.component('DatePicker', DatePicker);
