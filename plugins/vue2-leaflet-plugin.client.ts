import Vue from 'vue';
import * as Vue2Leaflet from 'vue2-leaflet';
import * as L from 'leaflet';
import Vue2LeafletMarkerCluster from 'vue2-leaflet-markercluster';
/*
Vue.component('l-circle', Vue2Leaflet.LCircle);
Vue.component('l-circle-marker', Vue2Leaflet.LCircleMarker);
Vue.component('l-control-attribution', Vue2Leaflet.LControlAttribution);
Vue.component('l-control-layers', Vue2Leaflet.LControlLayers);
Vue.component('l-control-scale', Vue2Leaflet.LControlScale);
Vue.component('l-control-zoom', Vue2Leaflet.LControlZoom);
Vue.component('l-feature-group', Vue2Leaflet.LFeatureGroup);
Vue.component('l-geo-json', Vue2Leaflet.LGeoJson);
Vue.component('l-icon', Vue2Leaflet.LIcon);
Vue.component('l-icon-default', Vue2Leaflet.LIconDefault);
Vue.component('l-image-overlay', Vue2Leaflet.LImageOverlay);
Vue.component('l-layer-group', Vue2Leaflet.LLayerGroup); */
Vue.component('LMap', Vue2Leaflet.LMap);
Vue.component('LMarker', Vue2Leaflet.LMarker);
Vue.component('LTileLayer', Vue2Leaflet.LTileLayer);
// Vue.component('l-geosearch', Vue2LeafletGeosearch);
Vue.component('VMarkerCluster', Vue2LeafletMarkerCluster);
Vue.component('LPolygon', Vue2Leaflet.LPolygon);
Vue.component('LPopup', Vue2Leaflet.LPopup);
/* Vue.component('l-polyline', Vue2Leaflet.LPolyline);
Vue.component('l-rectangle', Vue2Leaflet.LRectangle); */
/* Vue.component('l-tooltip', Vue2Leaflet.LTooltip);
Vue.component('l-lwms-tile-layer', Vue2Leaflet.LWMSTileLayer); */

// @ts-ignore Build icon assets.
// eslint-disable-next-line no-underscore-dangle
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.imagePath = '';
L.Icon.Default.mergeOptions({
  iconRetinaUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/images/marker-icon-2x.png',
  iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/images/marker-icon.png',
  shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/images/marker-shadow.png',
});

const LeafletPlugin = {
  // eslint-disable-next-line no-shadow
  install (Vue: { prototype: { $L: typeof L; }; }) {
    // Expose Leaflet
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$L = L;
  },
};

Vue.use(LeafletPlugin);
