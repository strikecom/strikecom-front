import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themesAnimated from '@amcharts/amcharts4/themes/animated';
import am4themesDark from '@amcharts/amcharts4/themes/dark';
import am4themesMaterial from '@amcharts/amcharts4/themes/material';
import am4themesAmcharts from '@amcharts/amcharts4/themes/amcharts';
import am4themesAmchartsDark from '@amcharts/amcharts4/themes/amchartsdark';
import am4themesDataviz from '@amcharts/amcharts4/themes/dataviz';
import am4themesFrozen from '@amcharts/amcharts4/themes/frozen';
import am4themesKelly from '@amcharts/amcharts4/themes/kelly';
import am4themesMicrochart from '@amcharts/amcharts4/themes/microchart';
import am4themesMoonrisekingdom from '@amcharts/amcharts4/themes/moonrisekingdom';
import am4themesPatterns from '@amcharts/amcharts4/themes/patterns';
import am4themesSpiritedaway from '@amcharts/amcharts4/themes/spiritedaway';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodataCountries2 from '@amcharts/amcharts4-geodata/data/countries2';
import Vue from 'vue';

declare module 'vue/types/vue' {
    // eslint-disable-next-line no-shadow
    interface Vue {
        $am4core(): any
    }
}

Vue.prototype.$am4core = () => {
  return {
    am4core,
    am4charts,
    am4maps,
    am4geodataCountries2,
    am4themesAnimated,
    am4themesDark,
    am4themesMaterial,
    am4themesAmcharts,
    am4themesAmchartsDark,
    am4themesDataviz,
    am4themesFrozen,
    am4themesKelly,
    am4themesMicrochart,
    am4themesMoonrisekingdom,
    am4themesPatterns,
    am4themesSpiritedaway,
  };
};
