import 'jointjs/dist/joint.core.css';

const jointCore = require('jointjs/src/core.mjs');
const basic = require('jointjs/src/shapes/basic.mjs');
const standard = require('jointjs/src/shapes/standard.mjs');

export default {
  install (Vue: any) {
    const joint = jointCore;
    joint.shapes = { standard, basic };
    Object.defineProperty(Vue.prototype, '$joint', { value: joint });
  },
};
