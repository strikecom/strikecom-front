import { PluginObject } from 'vue';

declare namespace VueMQPlugin {
    interface Options {
        // The optional (self-maintained) moment instance
        moment?: any;
    }

    interface VueStatic {
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $mq: VueMQPlugin.VueStatic;
    }
}

interface VueMQ extends PluginObject<undefined> {}

declare const VueMQ: VueMQ;
export = VueMQ;
