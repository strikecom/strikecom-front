import getAppRoutes from './utils/getRoutes';

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Забастком - агрегатор новостей о трудовых конфликтах',
    script: [
      {
        'data-goatcounter': 'https://zbc.goatcounter.com/count',
        src: '//gc.zgo.at/count.js',
        defer: true,
        async: true,
      },
    ],
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Проект нацелен на поддержку рабочего движения и освещение новостей о событиях, причиной которых послужил конфликт интересов работодателя и наемных работников.',
      },
      // Twitter
      // Test on: https://cards-dev.twitter.com/validator
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary_large_image',
      },
      { hid: 'twitter:site', name: 'twitter:site', content: '@nuxt_js' },
      {
        hid: 'twitter:url',
        name: 'twitter:url',
        content: 'https://www.zabastcom.org/',
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: 'Забастком - агрегатор новостей о трудовых конфликтах',
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: 'Проект нацелен на поддержку рабочего движения и освещение новостей о событиях, причиной которых послужил конфликт интересов работодателя и наемных работников.',
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: 'https://www.zabastcom.org/imag/index-image.jpg',
      },
      // Open Graph
      // Test on: https://developers.facebook.com/tools/debug/
      { hid: 'og:site_name', property: 'og:site_name', content: 'Zabastcom' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      {
        hid: 'og:url',
        property: 'og:url',
        content: 'https://www.zabastcom.org/',
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: 'Забастком - агрегатор новостей о трудовых конфликтах',
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: 'Проект нацелен на поддержку рабочего движения и освещение новостей о событиях, причиной которых послужил конфликт интересов работодателя и наемных работников.',
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://www.zabastcom.org/imag/index-image.jpg',
      },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: 'https://www.zabastcom.org/imag/index-image.jpg',
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: 'Strikecom',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css',
    'leaflet.markercluster/dist/MarkerCluster.css',
    'leaflet.markercluster/dist/MarkerCluster.Default.css',
  ],
  serverMiddleware: [
    '~/serverMiddleware/seo.ts',
  ],
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~/plugins/fontawesome.ts',
    '~/plugins/vue-moment.ts',
    // '~/plugins/jointJsPlugin.ts',
    '~/plugins/vue2-datepeaker-plugin.client.ts',
    '~/plugins/vue2-leaflet-plugin.client.ts',
    '~/plugins/axios-accessor.ts',
    { src: '@/plugins/vue-shortkey.ts', mode: 'client' },
    {
      src: '~/plugins/vuelidate.ts',
      ssr: true,
    },
    {
      src: '~/plugins/amcharts.ts',
      ssr: false,
    },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/vuetify',
  ],
  vuetify: {
    theme: { dark: false, disabled: true },
    treeShake: true,
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/sitemap',
    '@nuxtjs/style-resources',
    ['nuxt-leaflet', { /* module options */ }],
    'vue-social-sharing/nuxt',
    [
      'nuxt-mq',
      {
        // Default breakpoint for SSR
        defaultBreakpoint: 'default',
        breakpoints: {
          xsm: 420,
          msm: 540,
          sm: 850,
          md: 1220,
          lg: Infinity,
        },
      },
    ],
    ['@nuxtjs/firebase'],
    ['nuxt-font-loader-strategy', {
      ignoreLighthouse: true,
      ignoredEffectiveTypes: ['2g', 'slow-2g'],
      fonts: [
        // Font
        {
          fileExtensions: ['woff2', 'woff'],
          fontFamily: 'Futura New',
          fontFaces: [
            // Font-Face
            {
              preload: true,
              localSrc: ['Futura New Demi Reg', 'Futura-New-Demi-Reg'],
              src: '@/assets/fonts/FuturaNew/FuturaNewDemi-Reg/FuturaNewDemi-Reg',
              fontWeight: 500,
              fontStyle: 'normal',
            },
          ],
        },
        // Font
        {
          fileExtensions: ['woff2', 'woff'],
          fontFamily: 'San Francisco Pro',
          fontFaces: [
            {
              preload: true,
              localSrc: ['San Francisco Pro Display Bold', 'San-Francisco-Pro-Display-Bold'],
              src: '@/assets/fonts/SFProDisplay/SFProDisplay-Bold/SFProDisplay-Bold',
              fontWeight: 700,
              fontStyle: 'normal',
            },
            // Font-Face
            {
              preload: true,
              localSrc: ['San Francisco Pro Display Regular', 'San-Francisco-Pro-Display-Regular'],
              src: '@/assets/fonts/SFProDisplay/SFProDisplay-Regular/SFProDisplay-Regular',
              fontWeight: 400,
              fontStyle: 'normal',
            },
          ],
        },
      ],
    }],
  ],

  // Style-resources module configuration (https://webinmind.ru/nuxtjs/moduli/nuxt-style-resources)
  styleResources: {
    less: [
      '@/assets/theme/default/index.less',
      '@/assets/theme/default/variables.less',
      '@/assets/theme/default/keyframes.less',
    ],
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},
  sitemap: {
    routes() {
      return getAppRoutes();
    },
    path: '/sitemap.xml',
    gzip: true,
    generate: false,
  },
  firebase: {
    config: {
      apiKey: 'AIzaSyBqMWYu8ke0r0ONrI3SKR2C2h2Q4N261vc',
      authDomain: 'strikecom-7ad08.firebaseapp.com',
      databaseURL: 'https://strikecom-7ad08.firebaseio.com',
      projectId: 'strikecom-7ad08',
      storageBucket: 'strikecom-7ad08.appspot.com',
      messagingSenderId: '438907362423',
      appId: '1:438907362423:web:c011a0910bdd98267e3a72',
    },
    services: {
      auth: true,
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: ['vuelidate'],
  },
};
