export default function (req:any, res:any, next:any) {
  if ((req.url.length > 1) && (req.url[req.url.length - 1] === '/')) {
    res.writeHead(301, { Location: `/${req.url.slice(1, (req.url.length - 1))}` });
    res.end();
  } else {
    next();
  }
}
