module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended',
    'plugin:jest/recommended',
    'plugin:vue/essential', // this is a default sub-set of rules for your .vue files
    'airbnb-base', // plug airbnb rules
    '@vue/typescript', // default typescript related rules
  ],
  plugins: [
    'babel',
    'import',
    'prettier',
    '@typescript-eslint',
  ],
  parserOptions: {
    parser: '@typescript-eslint/parser', // the typescript-parser for eslint, instead of tslint
    sourceType: 'module', // allow the use of imports statements
    ecmaVersion: 2020, // allow the parsing of modern ecmascript
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'linebreak-style': 'off', // Неправильно работает в Windows.
    'no-unused-expressions': 'warn',
    'no-trailing-spaces': 'warn',
    indent: 'warn',
    quotes: 'warn',
    'comma-dangle': 'warn',
    'no-extraneous-dependencies': 'off',
    'no-const-assign': 'warn',
    'no-this-before-super': 'warn',
    'import/no-unresolved': 'off',
    'no-undef': 'warn',
    'no-unreachable': 'warn',
    'no-unused-vars': 'warn',
    'constructor-super': 'warn',
    'valid-typeof': 'warn',
    'lines-between-class-members': 'warn',
    semi: 'warn',
    'arrow-parens': 'off', // Несовместимо с prettier
    'object-curly-newline': 'off', // Несовместимо с prettier
    'no-mixed-operators': 'off', // Несовместимо с prettier
    'arrow-body-style': 'off',
    'function-paren-newline': 'off', // Несовместимо с prettier
    'no-plusplus': 'off',
    'space-before-function-paren': 0, // Несовместимо с prettier
    'max-len': ['off', { ignoreTemplateLiterals: true, ignoreStrings: true }],

  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.vue'],
        paths: ['components', 'layouts'],
      },
    },
  },
};
