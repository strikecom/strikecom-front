import { CountryInterface } from '@/components/api';

export const countriesMap: Map<string, number> = new Map([
  ['Турция', 19],
  ['Грузия', 10],
  ['Туркменистан', 12],
  ['Литва', 13],
  ['Таджикистан', 8],
  ['Беларусь', 4],
  ['Монголия', 18],
  ['Армения', 3],
  ['Латвия', 14],
  ['Украина', 11],
  ['Эстония', 15],
  ['Киргизия', 17],
  ['Россия', 1],
  ['Республика Корея', 16],
  ['Казахстан', 5],
  ['Молдова', 7],
  ['Азербайджан', 2],
  ['Узбекистан', 9],
]);

export const regionMap: CountryInterface[] = [
  {
    countryName: 'Russia',
    russianName: 'Россия',
    countryId: 'RU',
    regions: [
      {
        regionId: 'RU-ZAB',
        regionName: 'Zabaykalsky',
        regionRussianName: 'Забайкальский край',
      },
      {
        regionId: 'RU-YEV',
        regionName: 'Yevrey',
        regionRussianName: 'Еврейская автономная область',
      },
      {
        regionId: 'RU-YAR',
        regionName: 'Yaroslavl',
        regionRussianName: 'Ярославская область',
      },
      {
        regionId: 'RU-YAN',
        regionName: 'Yamalo-Nenets',
        regionRussianName: 'Ямало-Ненецкий АО',
      },
      {
        regionId: 'RU-VOR',
        regionName: 'Voronezh',
        regionRussianName: 'Воронежская область',
      },
      {
        regionId: 'RU-VLG',
        regionName: 'Vologda',
        regionRussianName: 'Вологодская область',
      },
      {
        regionId: 'RU-VGG',
        regionName: 'Volgograd',
        regionRussianName: 'Волгоградская область',
      },
      {
        regionId: 'RU-VLA',
        regionName: 'Vladimir',
        regionRussianName: 'Владимирская область',
      },
      {
        regionId: 'RU-ULY',
        regionName: 'Ulyanovsk',
        regionRussianName: 'Ульяновская область',
      },
      {
        regionId: 'RU-UD',
        regionName: 'Udmurt',
        regionRussianName: 'Удмуртская Республика',
      },
      {
        regionId: 'RU-TYU',
        regionName: 'Tyumen',
        regionRussianName: 'Тюменская область',
      },
      {
        regionId: 'RU-TVE',
        regionName: 'Tver',
        regionRussianName: 'Тверская область',
      },
      {
        regionId: 'RU-TY',
        regionName: 'Tuva',
        regionRussianName: 'Тыва',
      },
      {
        regionId: 'RU-TUL',
        regionName: 'Tula',
        regionRussianName: 'Тульская область',
      },
      {
        regionId: 'RU-TOM',
        regionName: 'Tomsk',
        regionRussianName: 'Томская область',
      },
      {
        regionId: 'RU-TA',
        regionName: 'Tatarstan',
        regionRussianName: 'Татарстан',
      },
      {
        regionId: 'RU-TAM',
        regionName: 'Tambov',
        regionRussianName: 'Тамбовская область',
      },
      {
        regionId: 'RU-SVE',
        regionName: 'Sverdlovsk',
        regionRussianName: 'Свердловская область',
      },
      {
        regionId: 'RU-STA',
        regionName: 'Stavropol',
        regionRussianName: 'Ставропольский край',
      },
      {
        regionId: 'RU-SMO',
        regionName: 'Smolensk',
        regionRussianName: 'Смоленская область',
      },
      {
        regionId: 'RU-SAR',
        regionName: 'Saratov',
        regionRussianName: 'Саратовская область',
      },
      {
        regionId: 'RU-SAM',
        regionName: 'Samara',
        regionRussianName: 'Самарская область',
      },
      {
        regionId: 'RU-SAK',
        regionName: 'Sakhalin',
        regionRussianName: 'Сахалинская область',
      },
      {
        regionId: 'RU-SA',
        regionName: 'Sakha',
        regionRussianName: 'Республика Саха (Якутия)',
      },
      {
        regionId: 'RU-RYA',
        regionName: 'Ryazan',
        regionRussianName: 'Рязанская область',
      },
      {
        regionId: 'RU-ROS',
        regionName: 'Rostov',
        regionRussianName: 'Ростовская область',
      },
      {
        regionId: 'RU-PSK',
        regionName: 'Pskov',
        regionRussianName: 'Псковская область',
      },
      {
        regionId: 'RU-PRI',
        regionName: 'Primorsky',
        regionRussianName: 'Приморский край',
      },
      {
        regionId: 'RU-PER',
        regionName: 'Perm',
        regionRussianName: 'Пермский край',
      },
      {
        regionId: 'RU-PNZ',
        regionName: 'Penza',
        regionRussianName: 'Пензенская область',
      },
      {
        regionId: 'RU-ORE',
        regionName: 'Orenburg',
        regionRussianName: 'Оренбургская область',
      },
      {
        regionId: 'RU-ORL',
        regionName: 'Oryol',
        regionRussianName: 'Орловская область',
      },
      {
        regionId: 'RU-OMS',
        regionName: 'Omsk',
        regionRussianName: 'Омская область',
      },
      {
        regionId: 'RU-NVS',
        regionName: 'Novosibirsk',
        regionRussianName: 'Новосибирская область',
      },
      {
        regionId: 'RU-NGR',
        regionName: 'Novgorod',
        regionRussianName: 'Новгородская область',
      },
      {
        regionId: 'RU-SE',
        regionName: 'North Ossetia-Alania',
        regionRussianName: 'Северная Осетия - Алания',
      },
      {
        regionId: 'RU-NIZ',
        regionName: 'Nizhegorod',
        regionRussianName: 'Нижегородская область',
      },
      {
        regionId: 'RU-NEN',
        regionName: 'Nenets',
        regionRussianName: 'Ненецкий АО',
      },
      {
        regionId: 'RU-MUR',
        regionName: 'Murmansk',
        regionRussianName: 'Мурманская область',
      },
      {
        regionId: 'RU-MOS',
        regionName: 'Moskva',
        regionRussianName: 'Московская область',
      },
      {
        regionId: 'RU-MOW',
        regionName: 'Moscow City',
        regionRussianName: 'Город федерального значения Москва',
      },
      {
        regionId: 'RU-MO',
        regionName: 'Mordovia',
        regionRussianName: 'Мордовия',
      },
      {
        regionId: 'RU-ME',
        regionName: 'Mariy-El',
        regionRussianName: 'Марий Эл',
      },
      {
        regionId: 'RU-MAG',
        regionName: 'Magadan',
        regionRussianName: 'Магаданская область',
      },
      {
        regionId: 'RU-LIP',
        regionName: 'Lipetsk',
        regionRussianName: 'Липецкая область',
      },
      {
        regionId: 'RU-LEN',
        regionName: 'Leningrad',
        regionRussianName: 'Ленинградская область',
      },
      {
        regionId: 'RU-KRS',
        regionName: 'Kursk',
        regionRussianName: 'Курская область',
      },
      {
        regionId: 'RU-KGN',
        regionName: 'Kurgan',
        regionRussianName: 'Курганская область',
      },
      {
        regionId: 'RU-KYA',
        regionName: 'Krasnoyarsk',
        regionRussianName: 'Красноярский край',
      },
      {
        regionId: 'RU-KDA',
        regionName: 'Krasnodar',
        regionRussianName: 'Краснодарский край',
      },
      {
        regionId: 'RU-KOS',
        regionName: 'Kostroma',
        regionRussianName: 'Костромская область',
      },
      {
        regionId: 'RU-KO',
        regionName: 'Komi',
        regionRussianName: 'Коми',
      },
      {
        regionId: 'RU-KIR',
        regionName: 'Kirov',
        regionRussianName: 'Кировская область',
      },
      {
        regionId: 'RU-KHM',
        regionName: 'Khanty-Mansiysk',
        regionRussianName: 'Ханты-Мансийский Автономный округ - Югра АО',
      },
      {
        regionId: 'RU-KK',
        regionName: 'Khakassia',
        regionRussianName: 'Хакасия',
      },
      {
        regionId: 'RU-KHA',
        regionName: 'Khabarovsk',
        regionRussianName: 'Хабаровский край',
      },
      {
        regionId: 'RU-KEM',
        regionName: 'Kemerovo',
        regionRussianName: 'Кемеровская область',
      },
      {
        regionId: 'RU-KR',
        regionName: 'Karelia',
        regionRussianName: 'Карелия',
      },
      {
        regionId: 'RU-KC',
        regionName: 'Karachay-Cherkess',
        regionRussianName: 'Карачаево-Черкесская Республика',
      },
      {
        regionId: 'RU-KAM',
        regionName: 'Kamchatka',
        regionRussianName: 'Камчатский край',
      },
      {
        regionId: 'RU-KLU',
        regionName: 'Kaluga',
        regionRussianName: 'Калужская область',
      },
      {
        regionId: 'RU-KL',
        regionName: 'Kalmykia',
        regionRussianName: 'Калмыкия',
      },
      {
        regionId: 'RU-KGD',
        regionName: 'Kaliningrad',
        regionRussianName: 'Калининградская область',
      },
      {
        regionId: 'RU-KB',
        regionName: 'Kabardino-Balkar',
        regionRussianName: 'Кабардино-Балкарская Республика',
      },
      {
        regionId: 'RU-IVA',
        regionName: 'Ivanovo',
        regionRussianName: 'Ивановская область',
      },
      {
        regionId: 'RU-IRK',
        regionName: 'Irkutsk',
        regionRussianName: 'Иркутская область',
      },
      {
        regionId: 'RU-IN',
        regionName: 'Ingushetia',
        regionRussianName: 'Ингушетия',
      },
      {
        regionId: 'RU-AL',
        regionName: 'Altay',
        regionRussianName: 'Алтай',
      },
      {
        regionId: 'RU-DA',
        regionName: 'Dagestan',
        regionRussianName: 'Дагестан',
      },
      {
        regionId: 'RU-SPE',
        regionName: 'St. Petersburg',
        regionRussianName: 'Город федерального значения Санкт-Петербург',
      },
      {
        regionId: 'RU-CU',
        regionName: 'Chuvash',
        regionRussianName: 'Чувашская Республика',
      },
      {
        regionId: 'RU-CHU',
        regionName: 'Chukotka',
        regionRussianName: 'Чукотский автономный округ',
      },
      {
        regionId: 'RU-CHE',
        regionName: 'Chelyabinsk',
        regionRussianName: 'Челябинская область',
      },
      {
        regionId: 'RU-CE',
        regionName: 'Chechnya',
        regionRussianName: 'Чеченская Республика',
      },
      {
        regionId: 'RU-BU',
        regionName: 'Buryatiya',
        regionRussianName: 'Бурятия',
      },
      {
        regionId: 'RU-BRY',
        regionName: 'Bryansk',
        regionRussianName: 'Брянская область',
      },
      {
        regionId: 'RU-BEL',
        regionName: 'Belgorod',
        regionRussianName: 'Белгородская область',
      },
      {
        regionId: 'RU-BA',
        regionName: 'Bashkortostan',
        regionRussianName: 'Республика Башкортостан',
      },
      {
        regionId: 'RU-AST',
        regionName: 'Astrakhan',
        regionRussianName: 'Астраханская область',
      },
      {
        regionId: 'RU-ARK',
        regionName: 'Arkhangelsk',
        regionRussianName: 'Архангельская область',
      },
      {
        regionId: 'RU-AMU',
        regionName: 'Amur',
        regionRussianName: 'Амурская область',
      },
      {
        regionId: 'RU-ALT',
        regionName: 'Altay Kray',
        regionRussianName: 'Алтайский край',
      },
      {
        regionId: 'RU-AD',
        regionName: 'Adygeya',
        regionRussianName: 'Адыгея',
      },
    ],
  },
  {
    countryName: 'Ukraine',
    countryId: 'UA',
    russianName: 'Украина',
    regions: [
      {
        regionId: 'UA-05',
        regionName: 'Vinnytsia',
        regionRussianName: 'Винницкая область',
      },
      {
        regionId: 'UA-07',
        regionName: 'Volyn',
        regionRussianName: 'Волынская область',
      },
      {
        regionId: 'UA-12',
        regionName: 'Dnipropetrovsk',
        regionRussianName: 'Днепропетровская область',
      },
      {
        regionId: 'UA-18',
        regionName: 'Zhytomyr',
        regionRussianName: 'Житомирская область',
      },
      {
        regionId: 'UA-21',
        regionName: 'Zakarpattia',
        regionRussianName: 'Закарпатская область',
      },
      {
        regionId: 'UA-23',
        regionName: 'Zaporizhzhya',
        regionRussianName: 'Запорожская область',
      },
      {
        regionId: 'UA-26',
        regionName: 'Ivano-Frankivsk',
        regionRussianName: 'Ивано-Франковская область',
      },
      {
        regionId: 'UA-30',
        regionName: 'Kyiv City',
        regionRussianName: 'Киев',
      },
      {
        regionId: 'UA-32',
        regionName: 'Kyiv',
        regionRussianName: 'Киевская область',
      },
      {
        regionId: 'UA-35',
        regionName: 'Kirovohrad',
        regionRussianName: 'Кировоградская область',
      },
      {
        regionId: 'UA-46',
        regionName: 'Lviv',
        regionRussianName: 'Львовская область',
      },
      {
        regionId: 'UA-48',
        regionName: 'Mykolaiv',
        regionRussianName: 'Николаевская область',
      },
      {
        regionId: 'UA-51',
        regionName: 'Odessa',
        regionRussianName: 'Одесская область',
      },
      {
        regionId: 'UA-53',
        regionName: 'Poltava',
        regionRussianName: 'Полтавская область',
      },
      {
        regionId: 'UA-56',
        regionName: 'Rivne',
        regionRussianName: 'Ровненская область',
      },
      {
        regionId: 'UA-59',
        regionName: 'Sumy',
        regionRussianName: 'Сумская область',
      },
      {
        regionId: 'UA-61',
        regionName: 'Ternopil',
        regionRussianName: 'Тернопольская область',
      },
      {
        regionId: 'UA-63',
        regionName: 'Kharkiv',
        regionRussianName: 'Харьковская область',
      },
      {
        regionId: 'UA-65',
        regionName: 'Kherson',
        regionRussianName: 'Херсонская область',
      },
      {
        regionId: 'UA-68',
        regionName: 'Khmelnytskyi',
        regionRussianName: 'Хмельницкая область',
      },
      {
        regionId: 'UA-71',
        regionName: 'Cherkasy',
        regionRussianName: 'Черкасская область',
      },
      {
        regionId: 'UA-74',
        regionName: 'Chernihiv',
        regionRussianName: 'Черниговская область',
      },
      {
        regionId: 'UA-77',
        regionName: 'Chernivtsi',
        regionRussianName: 'Черновицкая область',
      },
    ],
  },
  {
    countryName: 'Georgia',
    countryId: 'GE',
    russianName: 'Грузия',
    regions: [
      {
        regionId: 'GE-AJ',
        regionName: 'Ajaria',
        regionRussianName: 'Аджария',
      },
      {
        regionId: 'GE-GU',
        regionName: 'Guria',
        regionRussianName: 'Гурийский',
      },
      {
        regionId: 'GE-IM',
        regionName: 'Imereti',
        regionRussianName: 'край Имеретия',
      },
      {
        regionId: 'GE-KA',
        regionName: 'K\'akheti',
        regionRussianName: 'Кахетинский',
      },
      {
        regionId: 'GE-KK',
        regionName: 'Kvemo Kartli',
        regionRussianName: 'край Квемо Картли',
      },
      {
        regionId: 'GE-MM',
        regionName: 'Mtskheta-Mtianeti',
        regionRussianName: 'Мцхета-Мтианетский',
      },
      {
        regionId: 'GE-RL',
        regionName: 'Рача-Лечхуми-Квемо Сванети', // TODO
        regionRussianName: '',
      },
      {
        regionId: 'GE-SJ',
        regionName: 'Samtskhe-Javakheti',
        regionRussianName: 'Самцхе-Джавахетский',
      },
      {
        regionId: 'GE-SK',
        regionName: 'Shida Kartli',
        regionRussianName: 'Шида-Картлийский',
      },
      {
        regionId: 'GE-SZ',
        regionName: 'Samegrelo-Zemo Svaneti',
        regionRussianName: 'Самегрело и Земо-Сванетский',
      },
      {
        regionId: 'GE-TB',
        regionName: 'Tbilisi',
        regionRussianName: 'муниципалитет Тбилиси',
      },
    ],
  },
  {
    countryName: 'Kazakhstan',
    countryId: 'KZ',
    russianName: 'Казахстан',
    regions: [
      {
        regionId: 'KZ-AKM',
        regionName: 'Aqmola oblysy',
        regionRussianName: 'Акмолинская область',
      },
      {
        regionId: 'KZ-AKT',
        regionName: 'Aqtöbe oblysy',
        regionRussianName: 'Актюбинская область',
      },
      {
        regionId: 'KZ-ALM',
        regionName: 'Almaty oblysy',
        regionRussianName: 'Алматинская область',
      },
      {
        regionId: 'KZ-ATY',
        regionName: 'Atyraū oblysy',
        regionRussianName: 'Атырауская область',
      },
      {
        regionId: 'KZ-KAR',
        regionName: 'Qaraghandy oblysy',
        regionRussianName: 'Карагандинская область',
      },
      {
        regionId: 'KZ-KUS',
        regionName: 'Qostanay oblysy',
        regionRussianName: 'Костанайская область',
      },
      {
        regionId: 'KZ-KZY',
        regionName: 'Qyzylorda oblysy',
        regionRussianName: 'Кызылординская область',
      },
      {
        regionId: 'KZ-MAN',
        regionName: 'Mangghystaū oblysy',
        regionRussianName: 'Мангистауская область',
      },
      {
        regionId: 'KZ-PAV',
        regionName: 'Pavlodar oblysy',
        regionRussianName: 'Павлодарская область',
      },
      {
        regionId: 'KZ-SEV',
        regionName: 'Soltüstik Qazaqstan oblysy',
        regionRussianName: 'Северо-Казахстанская область',
      },
      {
        regionId: 'KZ-VOS',
        regionName: 'Shyghys Qazaqstan oblysy',
        regionRussianName: 'Восточно-Казахстанская область',
      },
      {
        regionId: 'KZ-YUZ',
        regionName: 'Ongtüstik Qazaqstan oblysy',
        regionRussianName: 'Туркестанская область',
      },
      {
        regionId: 'KZ-ZAP',
        regionName: 'Batys Qazaqstan oblysy',
        regionRussianName: 'Западно-Казахстанская область',
      },
      {
        regionId: 'KZ-ZHA',
        regionName: 'Zhambyl oblysy',
        regionRussianName: 'Жамбылская  область',
      },
      {
        regionId: 'KZ-ALS',
        regionName: 'Aral Sea',
        regionRussianName: 'Аральский район',
      },
    ],
  },
  {
    countryName: 'Azerbaijan',
    countryId: 'AZ',
    russianName: 'Азербайджан', // TODO
    regions: [
      {
        regionId: 'AZ-SAR',
        regionName: 'Sharur',
        regionRussianName: 'Шарурский район',
      },
    ],
  },
  {
    countryName: 'Armenia',
    countryId: 'AM',
    russianName: 'Армения',
    regions: [
      {
        regionId: 'AM-AG',
        regionName: 'Aragac̣otn',
        regionRussianName: 'Арагацотнская область',
      },
      {
        regionId: 'AM-AR',
        regionName: 'Ararat',
        regionRussianName: 'Араратская область',
      },
      {
        regionId: 'AM-AV',
        regionName: 'Armavir',
        regionRussianName: 'Армавирская область',
      },
      {
        regionId: 'AM-ER',
        regionName: 'Erevan',
        regionRussianName: 'Ереван',
      },
      {
        regionId: 'AM-GR',
        regionName: 'Gegark\'unik',
        regionRussianName: 'Гехаркуникская область',
      },
      {
        regionId: 'AM-KT',
        regionName: 'Kotayk',
        regionRussianName: 'Котайкская область',
      },
      {
        regionId: 'AM-LO',
        regionName: 'Loṙi',
        regionRussianName: 'Лорийская область',
      },
      {
        regionId: 'AM-SH',
        regionName: 'Širak',
        regionRussianName: 'Ширакская область',
      },
      {
        regionId: 'AM-SU',
        regionName: 'Syunik\'',
        regionRussianName: 'Сюникская область',
      },
      {
        regionId: 'AM-TV',
        regionName: 'Tavuš',
        regionRussianName: 'Тавушская область',
      },
      {
        regionId: 'AM-VD',
        regionName: 'Vayoć Jor',
        regionRussianName: 'Вайоцдзорская область',
      },
    ],
  },
  {
    countryName: 'Belarus',
    countryId: 'BY',
    russianName: 'Беларусь',
    regions: [
      {
        regionId: 'BY-BR',
        regionName: 'Brest',
        regionRussianName: 'Брестская область',
      },
      {
        regionId: 'BY-HM',
        regionName: 'Minsk City',
        regionRussianName: 'Минск',
      },
      {
        regionId: 'BY-HO',
        regionName: 'Homyel\'',
        regionRussianName: 'Гомельская область',
      },
      {
        regionId: 'BY-HR',
        regionName: 'Hrodna',
        regionRussianName: 'Гродненская область',
      },
      {
        regionId: 'BY-MA',
        regionName: 'Mahilyow',
        regionRussianName: 'Могилевская область',
      },
      {
        regionId: 'BY-MI',
        regionName: 'Minsk',
        regionRussianName: 'Минская область',
      },
      {
        regionId: 'BY-VI',
        regionName: 'Vitsyebsk',
        regionRussianName: 'Витебская область',
      },
    ],
  },
  {
    countryName: 'Tajikistan',
    countryId: 'TJ',
    russianName: 'Таджикистан',
    regions: [
      {
        regionId: 'TJ-DU',
        regionName: 'Dushanbe',
        regionRussianName: 'Душанбе', // TODO
      },
      {
        regionId: 'TJ-KT',
        regionName: 'Khatlon',
        regionRussianName: 'Хатлонская область',
      },
      {
        regionId: 'TJ-RA',
        regionName: 'nohiyahoi tobei jumhurí',
        regionRussianName: 'Районы республиканского подчинения',
      },
      {
        regionId: 'TJ-SU',
        regionName: 'Sughd',
        regionRussianName: 'Согдийская область',
      },
      {
        regionId: 'TJ-GB',
        regionName: 'Gorno-Badakhshan',
        regionRussianName: 'Горно-Бадахшанская автономная область',
      },
    ],
  },
  {
    countryName: 'Uzbekistan',
    countryId: 'UZ',
    russianName: 'Узбекистан',
    regions: [
      {
        regionId: 'UZ-AN',
        regionName: 'Andijon',
        regionRussianName: 'Андижанская область',
      },
      {
        regionId: 'UZ-BU',
        regionName: 'Buxoro',
        regionRussianName: 'Бухарская область',
      },
      {
        regionId: 'UZ-FA',
        regionName: 'Farg`ona',
        regionRussianName: 'Ферганская область',
      },
      {
        regionId: 'UZ-JI',
        regionName: 'Jizzax',
        regionRussianName: 'Джизакская область',
      },
      {
        regionId: 'UZ-NG',
        regionName: 'Namangan',
        regionRussianName: 'Наманганская область',
      },
      {
        regionId: 'UZ-NW',
        regionName: 'Navoiy',
        regionRussianName: 'Навоийская область',
      },
      {
        regionId: 'UZ-QA',
        regionName: 'Qashqadaryo',
        regionRussianName: 'Кашкадарьинская область',
      },
      {
        regionId: 'UZ-QR',
        regionName: 'Qoraqalpog‘iston',
        regionRussianName: 'Каракалпакия',
      },
      {
        regionId: 'UZ-SA',
        regionName: 'Samarqand',
        regionRussianName: 'Самаркандская область',
      },
      {
        regionId: 'UZ-SI',
        regionName: 'Sirdaryo',
        regionRussianName: 'Сырдарьинская область',
      },
      {
        regionId: 'UZ-SU',
        regionName: 'Surxondaryo',
        regionRussianName: 'Сурхандарьинская область',
      },
      {
        regionId: 'UZ-TK',
        regionName: 'Toshkent',
        regionRussianName: 'Ташкент',
      },
      {
        regionId: 'UZ-TO',
        regionName: 'Toshkent',
        regionRussianName: 'Ташкентская область',
      },
      {
        regionId: 'UZ-XO',
        regionName: 'Xorazm',
        regionRussianName: 'Хорезмская область',
      },
    ],
  },
  {
    countryName: 'Turkmenistan',
    countryId: 'TM',
    russianName: 'Туркменистан',
    regions: [
      {
        regionId: 'TM-S',
        regionName: 'Aşgabat',
        regionRussianName: 'Ашхабад',
      },
      {
        regionId: 'TM-B',
        regionName: 'Balkan',
        regionRussianName: 'Балканский велаят',
      },
      {
        regionId: 'TM-D',
        regionName: 'Daşoguz',
        regionRussianName: 'Дашогузский велаят',
      },
      {
        regionId: 'TM-A',
        regionName: 'Ahal',
        regionRussianName: 'Ахалский велаят',
      },
      {
        regionId: 'TM-M',
        regionName: 'Mary',
        regionRussianName: 'Марыйский велаят',
      },
      {
        regionId: 'TM-L',
        regionName: 'Lebap',
        regionRussianName: 'Лебапский велаят',
      },
    ],
  },
  {
    countryName: 'Lithuania',
    countryId: 'LT',
    russianName: 'Литва', // TODO
    regions: [
      {
        regionId: 'LT-KU',
        regionName: 'Kaunas',
        regionRussianName: 'Каунасский район',
      },
      {
        regionId: 'LT-VL',
        regionName: 'Vilnius',
        regionRussianName: 'Вильнюсское городское самоуправление',
      },
    ],
  },
  {
    countryName: 'Latvia',
    countryId: 'LV',
    russianName: 'Латвия', // TODO
    regions: [
      {
        regionId: 'LV-RIX',
        regionName: 'Rīga',
        regionRussianName: 'Рига',
      },
    ],
  },
  {
    countryName: 'Estonia',
    countryId: 'EE',
    russianName: 'Эстония', // TODO
    regions: [
      {
        regionId: 'EE-37',
        regionName: 'Harjumaa',
        regionRussianName: 'Харьюмаа',
      },
      {
        regionId: 'EE-44',
        regionName: 'Ida-Virumaa',
        regionRussianName: 'Ида-Вирумаа',
      },
      {
        regionId: 'EE-78',
        regionName: 'Tartumaa',
        regionRussianName: 'Тартумаа',
      },
    ],
  },
  {
    countryName: 'South Korea',
    countryId: 'KR',
    russianName: 'Республика Корея', // TODO
    regions: [
      {
        regionId: 'KR-26',
        regionName: 'Busan',
        regionRussianName: 'город-метрополия Пусан',
      },
    ],
  },
  {
    countryName: 'Kyrgyzstan',
    countryId: 'KG',
    russianName: 'Киргизия',
    regions: [
      {
        regionId: 'KG-Y',
        regionName: 'Ysyk-Köl',
        regionRussianName: 'Иссык-Кульская',
      },
      {
        regionId: 'KG-T',
        regionName: 'Talas',
        regionRussianName: 'Таласская',
      },
      {
        regionId: 'KG-GO',
        regionName: 'Osh',
        regionRussianName: '',
      },
      {
        regionId: 'KG-O',
        regionName: 'Osh',
        regionRussianName: 'Ошская',
      },
      {
        regionId: 'KG-N',
        regionName: 'Naryn',
        regionRussianName: 'Нарынская',
      },
      {
        regionId: 'KG-C',
        regionName: 'Chüy',
        regionRussianName: 'Чуйская',
      },
      {
        regionId: 'KG-GB',
        regionName: 'Bishkek',
        regionRussianName: 'Бишкек',
      },
      {
        regionId: 'KG-B',
        regionName: 'Batken',
        regionRussianName: 'Баткенская область',
      },
      {
        regionId: 'UZ-FA-CQ',
        regionName: 'Chon-Qora',
        regionRussianName: '',
      },
      {
        regionId: 'UZ-FA-SO',
        regionName: 'Sokh',
        regionRussianName: '',
      },
      {
        regionId: 'TJ-SU-V',
        regionName: 'Vorukh',
        regionRussianName: '',
      },
      {
        regionId: 'UZ-FA-SH',
        regionName: 'Shohimardon',
        regionRussianName: '',
      },
      {
        regionId: 'KG-J',
        regionName: 'Jalal-Abad',
        regionRussianName: 'Джалал-Абадская',
      },
    ],
  },
  {
    countryName: 'Mongolia',
    countryId: 'MN',
    russianName: 'Монголия', // TODO
    regions: [
      {
        regionId: 'MN-1',
        regionName: 'Ulaanbaatar',
        regionRussianName: 'Улан-Батор',
      },
    ],
  },
  {
    countryName: 'Turkey',
    countryId: 'TR',
    russianName: 'Турция', // TODO
    regions: [
      {
        regionId: 'TR-67',
        regionName: 'Zonguldak',
        regionRussianName: 'Зонгулдак',
      },
      {
        regionId: 'TR-67',
        regionName: 'Zonguldak',
        regionRussianName: 'Стамбул',
      },
    ],
  },
  {
    countryName: 'Moldova',
    countryId: 'MD',
    russianName: 'Молдова',
    regions: [
      {
        regionId: 'MD-AN',
        regionName: 'Anenii Noi',
        regionRussianName: 'Новоаненский район',
      },
      {
        regionId: 'MD-BA',
        regionName: 'Bălţi',
        regionRussianName: 'Бельцы',
      },
      {
        regionId: 'MD-BD',
        regionName: 'Tighina',
        regionRussianName: 'Бендеры',
      },
      {
        regionId: 'MD-BR',
        regionName: 'Briceni',
        regionRussianName: 'Бричанский район',
      },
      {
        regionId: 'MD-BS',
        regionName: 'Basarabeasca',
        regionRussianName: 'Бессарабский район',
      },
      {
        regionId: 'MD-CA',
        regionName: 'Cahul',
        regionRussianName: 'Кагульский район',
      },
      {
        regionId: 'MD-CL',
        regionName: 'Călăraşi',
        regionRussianName: 'Каларашский район',
      },
      {
        regionId: 'MD-CR',
        regionName: 'Criuleni',
        regionRussianName: 'Криулянский район',
      },
      {
        regionId: 'MD-CS',
        regionName: 'Căuşeni',
        regionRussianName: 'Каушанский район',
      },
      {
        regionId: 'MD-CT',
        regionName: 'Cantemir',
        regionRussianName: 'Кантемирский район',
      },
      {
        regionId: 'MD-CU',
        regionName: 'Chişinău',
        regionRussianName: 'Кишинёв',
      },
      {
        regionId: 'MD-CM',
        regionName: 'Cimişlia',
        regionRussianName: 'Чимишлийский район',
      },
      {
        regionId: 'MD-DO',
        regionName: 'Donduşeni',
        regionRussianName: 'Дондюшанский район',
      },
      {
        regionId: 'MD-DR',
        regionName: 'Drochia',
        regionRussianName: 'Дрокиевский район',
      },
      {
        regionId: 'MD-DU',
        regionName: 'Dubăsari',
        regionRussianName: 'Дубоссарский район',
      },
      {
        regionId: 'MD-ED',
        regionName: 'Edineţ',
        regionRussianName: 'Единецкий район',
      },
      {
        regionId: 'MD-FA',
        regionName: 'Făleşti',
        regionRussianName: 'Фалештский район',
      },
      {
        regionId: 'MD-FL',
        regionName: 'Floreşti',
        regionRussianName: 'Флорештский район',
      },
      {
        regionId: 'MD-GA',
        regionName: 'Unitatea Teritorială Autonomă Găgăuzia (UTAG)',
        regionRussianName: 'АТО Гагаузия',
      },
      {
        regionId: 'MD-GL',
        regionName: 'Glodeni',
        regionRussianName: 'Глодянский район',
      },
      {
        regionId: 'MD-HI',
        regionName: 'Hînceşti',
        regionRussianName: 'Хынчештский район',
      },
      {
        regionId: 'MD-IA',
        regionName: 'Ialoveni',
        regionRussianName: 'Яловенский район',
      },
      {
        regionId: 'MD-LE',
        regionName: 'Leova',
        regionRussianName: 'Леовский район',
      },
      {
        regionId: 'MD-NI',
        regionName: 'Nisporeni',
        regionRussianName: 'Ниспоренский район',
      },
      {
        regionId: 'MD-OC',
        regionName: 'Ocniţa',
        regionRussianName: 'Окницкий район',
      },
      {
        regionId: 'MD-OR',
        regionName: 'Orhei',
        regionRussianName: 'Оргеевский район',
      },
      {
        regionId: 'MD-RE',
        regionName: 'Rezina',
        regionRussianName: 'Резинский район',
      },
      {
        regionId: 'MD-RI',
        regionName: 'Rîşcani',
        regionRussianName: 'Рышканский район',
      },
      {
        regionId: 'MD-SD',
        regionName: 'Şoldăneşti',
        regionRussianName: 'Шолданештский район',
      },
      {
        regionId: 'MD-SI',
        regionName: 'Sîngerei',
        regionRussianName: 'Сынджерейский район',
      },
      {
        regionId: 'MD-SN',
        regionName: 'Unitatea Teritorială din Stînga Nistrului',
        regionRussianName: 'АТО Приднестровье',
      },
      {
        regionId: 'MD-SO',
        regionName: 'Soroca',
        regionRussianName: 'Сорокский район',
      },
      {
        regionId: 'MD-ST',
        regionName: 'Străşeni',
        regionRussianName: 'Страшенский район',
      },
      {
        regionId: 'MD-SV',
        regionName: 'Ştefan Vodă',
        regionRussianName: 'Штефан-Водский район',
      },
      {
        regionId: 'MD-TA',
        regionName: 'Taraclia',
        regionRussianName: 'Тараклийский район',
      },
      {
        regionId: 'MD-TE',
        regionName: 'Teleneşti',
        regionRussianName: 'Теленештский район',
      },
      {
        regionId: 'MD-UN',
        regionName: 'Ungheni',
        regionRussianName: 'Унгенский район',
      },
    ],
  },
];
