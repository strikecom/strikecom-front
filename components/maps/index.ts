export { countriesMap, regionMap } from '@/components/maps/regions';

export const eventStatusesMap: Map<string, number> = new Map([
  ['Завершенные конфликты', 1],
  ['Новые конфликты', 2],
  ['Конфликты в развитии', 3],
]);

export const eventTypesMap: Map<string, number> = new Map([
  ['Массовое увольнение', 8],
  ['Отказ от переработок', 7],
  ['Столкновение', 13],
  ['Угроза', 1],
  ['Переговоры', 14],
  ['Перекрытие магистралей', 4],
  ['Суд', 5],
  ['Расследование', 6],
  ['Забастовка', 12],
  ['ст. 142 ТК РФ', 2],
  ['Забастовка частичная', 3],
  ['Демонстрация', 10],
  ['Голодовка', 11],
  ['Обращение', 9],
  ['Саботаж', 29],
]);

// eslint-disable-next-line import/prefer-default-export
export const conflictReasonMap: Map<string, number> = new Map([
  ['Оплата труда', 1],
  ['Сокращения', 2],
  ['Ликвидация предприятия', 3],
  ['Прочее', 4],
  ['Увольнение', 5],
  ['Политика руководства', 6],
  ['Условия труда', 7],
  ['Рабочее время', 8],
  ['Коллективный договор', 9],
  ['Задержка ЗП', 10],
  ['Право на труд', 21],
]);

// eslint-disable-next-line import/prefer-default-export
export const conflictResultMap: Map<string, number> = new Map([
  ['Удовлетворены полностью', 1],
  ['Удовлетворены частично', 2],
  ['Не удовлетворены', 3],
  ['Результат неизвестен (либо в прогрессе)', 7],
]);

// eslint-disable-next-line import/prefer-default-export
export const environmentalVarsMap: Map<string, string> = new Map([
  ['domain', 'https://www.zabastcom.org'],
]);

// eslint-disable-next-line import/prefer-default-export
export const offerTypesMap: Map<string, string> = new Map([
  ['Новый конфликт', 'conflicts'],
  ['Событие в рамках конфликта', 'events'],
  ['Новость', 'news'],
]);

export const searchTypesMap: Map<string, string> = new Map([
  ['Конфликты', 'conflicts'],
  ['События', 'events'],
  ['Новости', 'news'],
]);

// eslint-disable-next-line import/prefer-default-export
export const conflictTypesMap: Map<string, number> = new Map([
  ['Массовое увольнение', 8],
  ['Отказ от переработок', 7],
  ['Столкновение', 13],
  ['Угроза', 1],
  ['Переговоры', 14],
  ['Перекрытие магистралей', 4],
  ['Суд', 5],
  ['Расследование', 6],
  ['Забастовка', 12],
  ['ст. 142 ТК РФ', 2],
  ['Забастовка частичная', 3],
  ['Демонстрация', 10],
  ['Голодовка', 11],
  ['Обращение', 9],
  ['Саботаж', 29],
]);

// eslint-disable-next-line import/prefer-default-export
export const industriesMap: Map<string, number> = new Map([
  ['Добывающая промышленность', 9],
  ['Культура и спорт', 7],
  ['Сельское хозяйство', 8],
  ['Госслужба', 5],
  ['Здравоохранение', 10],
  ['Строительство', 1],
  ['Транспорт', 2],
  ['Торговля', 3],
  ['Обрабатывающие производства', 4],
  ['ЖКХ', 6],
  ['Образование и наука', 11],
  ['Информационные технологии', 24],
]);

export const languagesMap: Map<string, string> = new Map([
  ['Русский', 'russian'],
  ['Английский', 'english'],
  ['Испанский', 'spanish'],
  ['Немецкий', 'german'],
]);

export const videoTypesMap: Map<string, number> = new Map([
  ['Youtube', 1],
  ['VK', 2],
  ['Другое', 3],
]);
