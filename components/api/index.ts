export interface NavItem {
    key: string;
    title: string;
    url: string;
    width?: number;
    imag?: string;
    disabled?: boolean;
}

export interface NavNetwork {
    key: string;
    shareKey?: string;
    img: string;
    width: number;
    member: number;
    url: string;
}

export interface IUser {
    id: number, // Идентификатор пользователя в системе.
    email: string, // Электронная почта пользователя.
    name: string, // Имя пользователя.
    imageUrl?: string, // Ссылка на аватар пользователя.
}

export interface ICountry {
    id: number, // Идентификатор страны.
}

export interface IRegion {
    id: number, // Идентификатор региона.
    name: string, // Имя региона. Не локализуется.
    country?: ICountry, // Информация о стране региона.
}

export interface ILocality {
    id: number, // Идентификатор населенного пункта.
    name: string, // Имя населенного пункта. Не локализуется.
    region?: IRegion, // Информация о регионе населенного пункта.
}

export interface IBriefEvent {
    id: number, // Идентификатор события.
    date?: number, // Дата события в формате timestamp.
    title: string, // Заголовок события. Формируется по правилам локализации.
}

export interface IRelatives {
    id: number, // Идентификатор конфликта.
    parentConflictId?: number, // Идентификатор родительского конфликта.
    parentEventId?: number, // Идентификатор родительского события. Другими словами ссылка на конкретное событие, породившее данный конфликт.
    events?: IBriefEvent[], // Массив событий конфликта, включая запрашиваемое событие. События отсортированы по дате.
}

export interface IVideo {
    previewUrl?: string; // Ссылка на изображение предпросмотра видео.
    url: string; // Ссылка на видео.
    videoTypeId?: number; // Идентификатор типа видео. Справочное значение.
}

export interface IConflict {
    id: number; // Идентификатор конфликта.
    mainTypeId?: number; // Тип конфликта. Справочное значение.
    parentEventId?: number; // Идентификатор события, которое породило данный конфликт. Если null, значит конфликт не имеет родительского события и является независимым.
    title: string; // Заголовок конфликта на русском. Формируется по правилам локализации.
    conflictResultId?: number; // Идентификатор результата конфликта. Справочное значение.
    conflictReasonId?: number; // Идентификатор причины конфликта. Справочное значение.
    industryId?: number; // Идентификатор отрасли конфликта. Справочное значение.
    companyName?: string; // Имя компании.
    createdAt: number; // Дата создания в формате timestamp.
    dateFrom?: number; // Дата начала конфликта в формате timestamp.
    dateTo?: number; // Дата окончания конфликта в формате timestamp. Если null, то конфликт не окончен.
    latitude?: number; // Координата широты конфликта.
    longitude?: number; // Координата долготы места конфликта.
    titleEs?: string; // Заголовок конфликта на испанском. Формируется по правилам локализации.
    titleEn?: string; // Заголовок конфликта на английском. Формируется по правилам локализации.
    titleDe?: string; // Заголовок конфликта на немецком. Формируется по правилам локализации.
}

export interface PostIConflict {
    mainTypeId?: number | null; // Тип конфликта. Справочное значение.
    parentEventId?: number | null; // Идентификатор события, которое породило данный конфликт. Если null, значит конфликт не имеет родительского события и является независимым.
    titleRu?: string | null; // Заголовок конфликта на русском. Формируется по правилам локализации.
    conflictResultId?: number | null; // Идентификатор результата конфликта. Справочное значение.
    conflictReasonId?: number | null; // Идентификатор причины конфликта. Справочное значение.
    industryId?: number | null; // Идентификатор отрасли конфликта. Справочное значение.
    companyName: string; // Имя компании.
    dateFrom: number; // Дата начала конфликта в формате timestamp.
    dateTo?: number | null; // Дата окончания конфликта в формате timestamp. Если null, то конфликт не окончен.
    latitude: number; // Координата широты места конфликта.
    longitude: number; // Координата долготы места конфликта.
    titleEs?: string | null; // Заголовок конфликта на испанском. Формируется по правилам локализации.
    titleEn?: string | null; // Заголовок конфликта на английском. Формируется по правилам локализации.
    titleDe?: string | null; // Заголовок конфликта на немецком. Формируется по правилам локализации.
}

export interface PostIEvent {
    date: number | null; // Дата новости.
    photoUrls?: string[]; // Массив ссылок на фотографии.
    videos?: IVideo[]; // Массив видео.
    sourceLink?: string; // Ссылка на источник.
    titleRu?: string | null; // Заголовок новости на русском. Формируется по правилам локализации.
    titleEs?: string | null; // Заголовок новости на испанском. Формируется по правилам локализации.
    titleEn?: string | null; // Заголовок новости на английском. Формируется по правилам локализации.
    titleDe?: string | null; // Заголовок новости на немецком. Формируется по правилам локализации.
    contentRu?: string | null; // Текст новости на русском. Формируется по правилам локализации.
    contentEs?: string | null; // Текст новости на испанском. Формируется по правилам локализации.
    contentEn?: string | null; // Текст новости на английском. Формируется по правилам локализации.
    contentDe?: string | null; // Текст новости на немецком. Формируется по правилам локализации.
    conflictId: number; // Идентификатор конфликта, которому принадлежит событие.
    eventTypeId?: number | null; // Идентификатор типа события. Справочное значение.
    localityId?: number | null; // Идентификатор страны события. Справочное значение.
    latitude: number; // Координата широты места события.
    longitude: number; // Координата долготы места события.
}

export interface PostINews {
    date: number | null; // Дата новости.
    photoUrls?: string[]; // Массив ссылок на фотографии.
    videos?: IVideo[]; // Массив видео.
    sourceLink?: string | null; // Ссылка на источник.
    titleRu?: string | null; // Заголовок новости на русском. Формируется по правилам локализации.
    titleEs?: string | null; // Заголовок новости на испанском. Формируется по правилам локализации.
    titleEn?: string | null; // Заголовок новости на английском. Формируется по правилам локализации.
    titleDe?: string | null; // Заголовок новости на немецком. Формируется по правилам локализации.
    contentRu?: string | null; // Текст новости на русском. Формируется по правилам локализации.
    contentEs?: string | null; // Текст новости на испанском. Формируется по правилам локализации.
    contentEn?: string | null; // Текст новости на английском. Формируется по правилам локализации.
    contentDe?: string | null; // Текст новости на немецком. Формируется по правилам локализации.
}

export interface IEvent {
    id: number; // Идентификатор события.
    title: string; // Заголовок события на русском. Формируется по правилам локализации.
    content: string; // Содержание события на русском. Формируется по правилам локализации.
    eventStatusId?: number; // Идентификатор статуса события. Справочное значение.
    eventTypeId?: number; // Идентификатор типа события. Справочное значение.
    conflictId: number; // Идентификатор конфликта, которому принадлежит событие.
    conflict: IConflict; // Информация о конфликте, которому принадлежит событие.
    createdAt: number; // Дата создания в формате timestamp.
    date?: number; // Дата события в формате timestamp.
    latitude?: number; // Координата широты места события.
    longitude?: number; // Координата долготы места события.
    sourceLink?: string; // Ссылка на источник.
    published: boolean; // Опубликовано ли событие.
    photoUrls: string[]; // Массив ссылок на фотографии.
    tags?: string[]; // Массив тэгов.
    videos?: IVideo[]; // Массив видео.
    views: number; // Количество просмотров события.
    titleEs?: string; // Заголовок события на испанском. Формируется по правилам локализации.
    titleEn?: string; // Заголовок события на английском. Формируется по правилам локализации.
    titleDe?: string; // Заголовок события на немецком. Формируется по правилам локализации.
    contentDe?: string; // Содержание события на немецком. Формируется по правилам локализации.
    contentEs?: string; // Содержание события на испанском. Формируется по правилам локализации.
    contentEn?: string; // Содержание события на английском. Формируется по правилам локализации.
}

export interface INews {
    id: number; // Идентификатор события.
    title: string; // Заголовок события на русском. Формируется по правилам локализации.
    content: string; // Содержание события на русском. Формируется по правилам локализации.
    conflict?: IConflict; // Информация о конфликте, которому принадлежит событие.
    createdAt: number; // Дата создания в формате timestamp.
    date?: number; // Дата события в формате timestamp.
    sourceLink?: string; // Ссылка на источник.
    published: boolean; // Опубликовано ли событие.
    photoUrls: string[]; // Массив ссылок на фотографии.
    tags?: string[]; // Массив тэгов.
    videos?: IVideo[]; // Массив видео.
    views: number; // Количество просмотров события.
    titleEs?: string; // Заголовок события на испанском. Формируется по правилам локализации.
    titleEn?: string; // Заголовок события на английском. Формируется по правилам локализации.
    titleDe?: string; // Заголовок события на немецком. Формируется по правилам локализации.
    contentDe?: string; // Содержание события на немецком. Формируется по правилам локализации.
    contentEs?: string; // Содержание события на испанском. Формируется по правилам локализации.
    contentEn?: string; // Содержание события на английском. Формируется по правилам локализации.
}

/*
Пример ответа:

{
    "data":[
        {
            "id":2196,
            "published":true,
            "date":1585749337,
            "views":9,
            "sourceLink":"https://4s-info.ru/2020/04/01/gornyaki-kuzbasskih-shaht-bankrotov-obratilis-k-putinu/",
            "photoUrls":[
                "https://4s-info.ru/wp-content/uploads/sites/2/2020/04/IMG-20200207-WA0014-1024x768.jpg"
            ],
            "videos":[],
            "tags":[],
            "latitude":54.59314,
            "longitude":86.2484,
            "conflictId":1207,
            "eventStatusId":2,
            "eventTypeId":9,
            "createdAt":1585749392,
            "conflict":{
                "id":1207,
                "latitude":54.59314,
                "longitude":86.2484,
                "companyName":"УК «Полысаевская»",
                "dateFrom":1585749139,
                "dateTo":null,
                "conflictReasonId":2,
                "conflictResultId":null,
                "industryId":9,
                "parentEventId":2139,
                "createdAt":1585749292,
                "titleEs":null,
                "titleEn":null,
                "titleDe":null,
                "titleRu":"Сокращения на шахтах «Алексиевская» и «Заречная»"
            },
            "contentRu":"На кузбасских шахтах «Алексиевская» и «Заречная» в апреле ожидаются массовые сокращения работников, разом лишаться работы более 500 горняков. Инициативная группа шахтеров и жителей региона обратилась в президенту России с просьбой о помощи.",
            "titleEs":null,
            "titleEn":null,
            "contentDe":null,
            "contentEs":null,
            "titleDe":null,
            "contentEn":null,
            "titleRu":" Обращение шахтёров «Алексиевская» и «Заречная»"
        }
    ],
    "meta":{
        "currentPage":1,
        "lastPage":6342,
        "perPage":1,
        "total":6342
    }
}
*/

export interface IDetailedEvent {
    author?: IUser; // Информация об авторе события.
    locality?: ILocality; // Информация о населенном пункте события.
    relatives?: IRelatives[]; // Информация о связанных событиях. Содержится в ответе только если в запросе был передан withRelatives: true.
    // Если событие принадлежит нескольким конфликтам, например событие на одном предприятии породило конфликт на другом предприятии, то в этом массиве будет несколько элементов.
    id: number; // Идентификатор события.
    title: string; // Заголовок события на русском. Формируется по правилам локализации.
    content: string; // Содержание события на русском. Формируется по правилам локализации.
    eventStatusId?: number; // Идентификатор статуса события. Справочное значение.
    eventTypeId?: number; // Идентификатор типа события. Справочное значение.
    conflictId: number; // Идентификатор конфликта, которому принадлежит событие.
    conflict: IConflict; // Информация о конфликте, которому принадлежит событие.
    createdAt: number; // Дата создания в формате timestamp.
    date?: number; // Дата события в формате timestamp.
    latitude?: number; // Координата широты места события.
    longitude?: number; // Координата долготы места события.
    sourceLink?: string; // Ссылка на источник.
    published: boolean; // Опубликовано ли событие.
    photoUrls: string[]; // Массив ссылок на фотографии.
    tags?: string[]; // Массив тэгов.
    videos?: IVideo[]; // Массив видео.
    views: number; // Количество просмотров события.
    titleEs?: string; // Заголовок события на испанском. Формируется по правилам локализации.
    titleEn?: string; // Заголовок события на английском. Формируется по правилам локализации.
    titleDe?: string; // Заголовок события на немецком. Формируется по правилам локализации.
    contentDe?: string; // Содержание события на немецком. Формируется по правилам локализации.
    contentEs?: string; // Содержание события на испанском. Формируется по правилам локализации.
    contentEn?: string; // Содержание события на английском. Формируется по правилам локализации.
}

export interface IDetailedNews {
    author?: IUser; // Информация об авторе новости.
    id: number; // Идентификатор новости.
    title: string; // Заголовок новости. Формируется по правилам локализации.
    content: string; // Содержание новости. Формируется по правилам локализации.
    conflict: IConflict; // Информация о конфликте, которому принадлежит событие.
    createdAt: number; // Дата создания в формате timestamp.
    date: number; // Дата новости в формате timestamp.
    sourceLink: string; // Ссылка на источник.
    published: boolean; // Опубликована ли новость.
    photoUrls: string[]; // Массив ссылок на фотографии.
    tags?: string[]; // Массив тэгов.
    videos?: IVideo[]; // Массив видео.
    views: number; // Количество просмотров новости.
}

/*
Пример ответа:
{
    "data": {
        "id": 789,
        "published": true,
        "date": 1552633226,
        "views": 62,
        "sourceLink": "https://www.kavkaz-uzel.eu/articles/332933/",
        "photoUrls": [],
        "videos": [
            {
                "url": "6juCJkE1ypM",
                "previewUrl": null,
                "videoTypeId": 1
            }
        ],
        "tags": [],
        "author": {
            "id": 400,
            "name": " Владимир Маяковский",
            "email": "zabastrikecom@gmail.com",
            "imageUrl": "https://i.imgur.com/CfUfNyJ.jpg"
        },
        "latitude": 48.038177,
        "longitude": 39.95774,
        "conflictId": 510,
        "eventStatusId": 3,
        "eventTypeId": 11,
        "conflict": {
            "id": 510,
            "latitude": 48.046585,
            "longitude": 39.978355,
            "companyName": null,
            "dateFrom": 1524700800,
            "dateTo": 1553078543,
            "conflictReasonId": 10,
            "conflictResultId": 3,
            "industryId": 9,
            "parentEventId": null,
            "createdAt": 1524700800,
            "titleEs": null,
            "titleEn": null,
            "titleDe": null,
            "titleRu": null
        },
        "locality": {
            "id": 21,
            "name": "Гуково",
            "region": {
                "id": 58,
                "name": "Ростовская область",
                "country": {
                    "id": 1,
                    "nameRu": "Россия",
                    "nameDe": null,
                    "nameEs": "Russia",
                    "nameEn": "Russia"
                }
            }
        },
        "createdAt": 1552633226,
        "contentRu": "\rТрое бывших работников «Кингкоула» и представитель протестующих Валерий Дьяконов третьи сутки голодают в Ростовской области, добиваясь от властей погашения долгов по зарплате. Пожилая участница голодовки сообщила об ухудшении самочувствия.",
        "titleEs": null,
        "titleEn": "The third day of the hunger strike of the employees of the \"Kingkoul\"",
        "contentDe": null,
        "contentEs": null,
        "relatives": [
            {
                "id": 510,
                "parentEventId": null,
                "parentConflictId": null,
                "events": [
                    {
                        "id": 26,
                        "date": 1524700800,
                        "titleRu": "Пикет у ДК шахты \"Ростовская\" в Гуково собрал 65 человек.",
                        "titleEn": null,
                        "titleEs": null,
                        "titleDe": null
                    },
                    {
                        "id": 174,
                        "date": 1537515936,
                        "titleRu": "Пикет возле ДК шахты 'Ростовская' в Гуково",
                        "titleEn": null,
                        "titleEs": null,
                        "titleDe": null
                    },
                    {
                        "id": 322,
                        "date": 1538810778,
                        "titleRu": "В Гуково прошёл пикет бывших шахтёров",
                        "titleEn": null,
                        "titleEs": null,
                        "titleDe": null
                    },
                    {
                        "id": 403,
                        "date": 1541187013,
                        "titleRu": "Пикет в Гуково бывших работников 'Кингкоул'",
                        "titleEn": null,
                        "titleEs": null,
                        "titleDe": null
                    },
                    {
                        "id": 531,
                        "date": 1541759797,
                        "titleRu": "В Гуково прошла встреча шахтёров",
                        "titleEn": null,
                        "titleEs": null,
                        "titleDe": null
                    }
                ]
            }
        ],
        "titleDe": null,
        "contentEn": "Three former employees of \"KingCoula\" and representative of the protesters Valery Dyakonov go hungry for three days, they demand repayment wages arrears.  An elderly participant of the hunger strike reported health deterioration.",
        "titleRu": "Третьи сутки голодовки работников «Кингкоула»"
    }
}
*/

export interface SwitchButton {
    key: string,
    title: string
}

export interface EventSort {
    field?: string,
    order?: string,
}

export interface NewsSort {
    field?: string,
    order?: string,
}

export interface EventNear {
    radius?: number,
    lat?: number,
    lng?: number,
}

export interface EventFilters {
    conflictIds?: number[],
    countryIds?: number[],
    regionIds?: number[],
    near?: EventNear,
    dateFrom?: number,
    dateTo?: number,
    eventStatusIds?: number[],
    eventTypeIds?: number[],
    fulltext?: string,
    industryIds?: number[],
    published?: boolean,
    tags?: [],
    favourites?: boolean,
}

export interface EventRequest {
    page: string,
    perPage: string,
    filters?: EventFilters,
    sort?: EventSort,
}

export interface EventPageContent {
    text: string | null,
    imag: string | null,
    video: string | null,
}

export interface VideoOptions {
    title: string,
    controls: string[],
    settings: string[],
}

export interface ConflictSort {
    field?: string,
    order?: string,
}

export interface ConflictFilters {
    ancestorsOf?: number,
    childrenOf?: number,
    conflictReasonIds?: number[],
    conflictResultIds?: number[],
    near?: EventNear,
    dateFrom?: number,
    dateTo?: number,
    fulltext?: string,
    mainTypeIds?: number[],
}

export interface NewsPageContent {
    text: string | null,
    imag: string | null,
    video: string | null,
}

export interface ConflictRequest {
    page: string,
    perPage: string,
    brief?: boolean, // Краткое описание
    filters?: ConflictFilters,
    sort?: ConflictSort,
}

export interface NewsFilters {
    dateFrom?: number,
    dateTo?: number,
    fulltext?: string,
    published?: boolean,
    tags?: [],
    favourites?: boolean,
}

export interface NewsRequest {
    page: string,
    perPage: string,
    filters?: NewsFilters,
    sort?: NewsSort,
}

export interface RegionRequest {
    countryId: number
}

export interface LocalityRequest {
    regionId: number
}

export interface AutocompleteItem {
    text: string | number | object,
    value?: string | number | object,
    disabled?: boolean,
    divider?: boolean,
    header?: string
}

export interface OfferTypeItem {
    text: string,
    value: string
}

export interface SearchTypeItem {
    text: string,
    value: string
}

export interface HeadObject {
    title: string,
    meta: {
        charset?: string,
        hid?: string,
        name?: string,
        property?: string,
        content?: string,
    }[]
}

export interface PostFavourite {
    favourite: boolean; // Избранное
}

export interface ReportRequest {
    countriesIds: number[],
    from?: string,
    to?: string,
}

export interface PieChartItem {
    name: string,
    value1: number,
    label1?: string,
    value2?: number,
    label2?: string,
}

export interface StackedColumnChartItem {
    name: string,
    [key: string]: number | string,
}
export interface CountryCount {
    countryId: number,
    count: number,
}

export interface IndustryCount {
    industryId: number | null,
    count: number,
}

export interface ReasonCount {
    reasonId: number,
    count: number,
}

export interface ResultCount {
    resultId: number,
    count: number,
}

export interface TypeCount {
    typeId: number,
    count: number,
}

export interface RegionCount {
  [key: string]: number,
}

export interface ReasonCountByIndustry {
    industryId: number | null,
    countByReason: ReasonCount[],
}

export interface ResultCountByIndustry {
    industryId: number | null,
    countByResult: ResultCount[],
}

export interface ResultCountByType {
    typeId: number,
    countByResult: ResultCount[],
}

export interface TypeCountByIndustry {
    industryId: number | null,
    countByType: TypeCount[],
}

export interface ReportResult {
    conflictsBeganBeforeDateFromCount: number,
    countByCountries: CountryCount[],
    countByIndustries: IndustryCount[],
    countByReasons: ReasonCount[],
    countByResults: ResultCount[],
    countByTypes: TypeCount[],
    countByReasonsByIndustries: ReasonCountByIndustry[],
    countByResultsByIndustries: ResultCountByIndustry[],
    countByResultsByTypes: ResultCountByType[],
    countByTypesByIndustries: TypeCountByIndustry[],
    countByRegions: RegionCount[],
    countByDistricts: Object,
    specificCountByDistricts: Object,
}


export interface ResponsiveDesignConfig {
  contentWidth: number,
  categoryAxisTruncate: boolean,
  categoryAxisFontSize: number,
  legendFontSize: number,
  labelBulletFontSize: number,
  legendMaxHeight: number,
}

export interface RegionInterface {
  regionId: string,
  regionName: string,
  regionRussianName: string,
}

export interface CountryInterface {
  countryId: string,
  countryName: string,
  russianName?: string,
  regions: RegionInterface[],
}
