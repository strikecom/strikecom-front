const axios = require('axios');

module.exports = async function getAppRoutes() {
  const dynamicRoutes = [];
  for (let i = 1; i < 100; i++) {
    // eslint-disable-next-line no-await-in-loop
    const events = (await axios.get(`https://zabastcom.org/api/v2/Ru/events?page=${i}&perPage=1000&filters.published=true`)).data;
    // eslint-disable-next-line no-await-in-loop
    const news = (await axios.get(`https://zabastcom.org/api/v2/Ru/news?page=${i}&perPage=1000&filters.published=true`)).data;
    // eslint-disable-next-line no-await-in-loop
    const conflicts = (await axios.get(`https://zabastcom.org/api/v2/Ru/conflicts?page=${i}&perPage=1000`)).data;
    dynamicRoutes.push(...events.data.map((event) => {
      return `/events/${event.id}`;
    }));
    dynamicRoutes.push(...news.data.map((newsItem) => {
      return `/news/${newsItem.id}`;
    }));
    dynamicRoutes.push(...conflicts.data.map((conflict) => {
      return `/conflicts/${conflict.id}`;
    }));
  }
  return dynamicRoutes;
};
