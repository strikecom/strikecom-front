import EventCard from '@/components/EventCard.vue';
import { createLocalVue, mount, RouterLinkStub, Wrapper } from '@vue/test-utils';
import { IEvent, INews } from '@/components/api';
import VueMoment from 'vue-moment';
import Vuex, { Store } from 'vuex';

import moment from 'moment';
import 'moment/locale/ru';

import { library, config } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import Vue from 'vue';

config.autoAddCss = false;
library.add(fas, fab, far);

describe('EventCard', () => {
  let wrapper: Wrapper<Vue, Element>;


  const localVue: typeof Vue = createLocalVue();
  localVue.use(VueMoment, { moment });
  localVue.use(Vuex);
  localVue.component('FontAwesomeIcon', FontAwesomeIcon);

  let actions;
  let state;
  let mutations;
  let store: Store<any>;

  beforeEach(() => {
    state = {
      userBookmarks: [],
      userConflictBookmarks: [],
      userNewsBookmarks: [],
    };
    mutations = {
      // eslint-disable-next-line no-shadow
      SET_BOOKMARKS(state: any, bookmarkIds: number[]) {
        // eslint-disable-next-line no-param-reassign
        state.userBookmarks = bookmarkIds;
      },

      // eslint-disable-next-line no-shadow
      ADD_BOOKMARK(state: any, id: number) {
        state.userBookmarks.push(id);
      },

      // eslint-disable-next-line no-shadow
      DELETE_BOOKMARK(state: any, id: number) {
        const index: number = state.userBookmarks.indexOf(id);
        if (index > -1) {
          state.userBookmarks.splice(index, 1);
        }
      },
    };
    actions = {
      async addBookmark({ commit }: any, { type, id }: {type: string, id: string}): Promise<void> {
        if (type === 'events') {
          commit('ADD_BOOKMARK', id);
        } else if (type === 'news') {
          commit('ADD_NEWS_BOOKMARK', id);
        } else {
          commit('ADD_CONFLICT_BOOKMARK', id);
        }
      },

      async deleteBookmark({ commit }: any, { type, id }: {type: string, id: string}): Promise<void> {
        if (type === 'events') {
          commit('DELETE_BOOKMARK', id);
        } else if (type === 'news') {
          commit('DELETE_NEWS_BOOKMARK', id);
        } else {
          commit('DELETE_CONFLICT_BOOKMARK', id);
        }
      },
    };
    store = new Vuex.Store({
      state,
      mutations,
      actions,
    });
  });

  const createComponent = (props: {
        iEvent: IEvent | INews,
        type: string,
        cardSize: string,
    }) => {
    wrapper = mount(EventCard, {
      localVue,
      store,
      stubs: {
        NuxtLink: RouterLinkStub,
      },
      propsData: props,
    });
  };

  const timestampParse = (timestamp: number): string => (timestamp ? moment(new Date(timestamp * 1000)).format('D MMMM, YYYY') : '');

  afterEach(() => {
    wrapper.destroy();
  });

  const I_EVENT: IEvent = {
    id: 4589,
    published: true,
    date: 1643032223,
    views: 3091,
    sourceLink: 'https://angarsk38.ru/news/my-protiv-kollektivy-angarskikh-bolnic-vyskazalis-po-voprosu-sliyaniya/',
    photoUrls: ['https://angarsk38.ru/wp-content/uploads/2022/01/iEWdSjTsapk-1024x1024.jpg'],
    videos: [],
    latitude: 52.5092,
    longitude: 103.85061,
    conflictId: 2495,
    eventStatusId: 1,
    eventTypeId: 14,
    conflict: {
      id: 2495,
      latitude: 52.5092,
      longitude: 103.85061,
      companyName: 'Ангарский перинатальный центр',
      dateFrom: 1642058899,
      dateTo: 1643032450,
      conflictReasonId: 6,
      conflictResultId: 2,
      industryId: 10,
      createdAt: 1642098541,
      mainTypeId: 9,
      title: 'Реорганизация Ангарского перинатального центра',
    },
    createdAt: 1643032522,
    title: 'Предложено приостановить реорганизацию ангарских больниц',
    content: 'Комитет по здравоохранению предложил правительству Иркутской области приостановить реорганизацию до завершения действия режима повышенной готовности, введённый из-за коронавируса. Коллективы ангарских больниц высказались против слияния.',
  };

  const I_NEWS: INews = {
    id: 719,
    published: true,
    date: 1642769538,
    views: 2225,
    sourceLink: 'https://www.vedomosti.ru/economics/articles/2022/01/20/905781-mintrud-povishenie',
    photoUrls: [],
    videos: [],
    tags: [],
    createdAt: 1642769616,
    title: 'Бизнес выступил против увеличения МРОТ',
    content: 'Бизнес выступил против предложения Федерации независимых профсоюзов России ежегодно увеличивать соотношение МРОТ и медианной зарплаты на 1 п. п. до 2030 г. Сейчас показатель МРОТ/медианная зарплата составляет 42% в целом по стране.',
  };

  const TYPE_EVENTS: string = 'events';
  const TYPE_NEWS: string = 'news';
  const CARD_SIZE_LARGE: string = 'large';
  const CARD_SIZE_SMALL: string = 'small';

  test('event card displays the title', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Action
    // await wrapper.vm.$nextTick();

    // Assert
    expect(wrapper.text()).toContain(I_EVENT.title);
  });

  test('event card displays the content', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    expect(wrapper.text()).toContain(I_EVENT.content);
  });

  test('event card content is cut off on small devices', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_SMALL,
    });

    // Assert
    expect(wrapper.text()).not.toContain(I_EVENT.content);
    expect(wrapper.text()).toContain(I_EVENT.content.split(' ')[0]);
  });

  test('event card displays the conflict title', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    expect(wrapper.text()).toContain(I_EVENT.conflict.title);
  });

  test('event card for news does not display the conflict title', () => {
    // Arrange
    createComponent({
      iEvent: I_NEWS,
      type: TYPE_NEWS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    expect(wrapper.text()).not.toContain(I_EVENT.conflict.title);
  });

  test('event card displays the event date', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    expect(wrapper.text()).toContain(timestampParse(I_EVENT.createdAt));
  });

  test('event card displays the number of views', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    expect(wrapper.text()).toContain(I_EVENT.views);
  });

  test('event card contains link to event page', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    const eventLink: string = `/events/${I_EVENT.id.toString()}`;
    const targetLink: string = wrapper.findAllComponents(RouterLinkStub).wrappers.find((link: Wrapper<any>) => {
      return link.text().includes(I_EVENT.title);
    })?.props().to || '';
    expect(targetLink).toBe(eventLink);
  });

  test('event card contains link to conflict', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    const conflictLink: string = `/conflicts/${I_EVENT.conflict.id.toString()}`;
    const targetLink: string = wrapper.findAllComponents(RouterLinkStub).wrappers.find((link: Wrapper<any>) => {
      return !link.text().includes(I_EVENT.title) && link.text().includes(I_EVENT.conflict.title);
    })?.props().to || '';
    expect(targetLink).toBe(conflictLink);
  });

  test('event card contains preview image if it specified', () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    expect(wrapper.find('img').attributes().src).toBe(I_EVENT.photoUrls[0]);
  });

  test('event card contains default image if preview image is not specified', () => {
    // Arrange
    createComponent({
      iEvent: I_NEWS,
      type: TYPE_NEWS,
      cardSize: CARD_SIZE_LARGE,
    });

    // Assert
    const newsDefaultImagUrl: string = '/imag/strikecom-news-red.webp';
    expect(wrapper.find('img').attributes().src).toBe(newsDefaultImagUrl);
  });

  test('event card bookmark switching is enabled', async () => {
    // Arrange
    createComponent({
      iEvent: I_EVENT,
      type: TYPE_EVENTS,
      cardSize: CARD_SIZE_LARGE,
    });
    const bookmarkButton: Wrapper<Vue, Element> | undefined = wrapper.findAll('div').wrappers.find((bookmark: Wrapper<any>) => {
      return bookmark.text().length === 0 && bookmark.attributes().tabindex === '0' && bookmark.findAll('svg').length;
    });
    const initialClassArr: string = bookmarkButton?.attributes().class || '';

    // Action
    await bookmarkButton?.trigger('click');

    // Assert
    const finalClassArr: string = bookmarkButton?.attributes().class || '';
    expect(initialClassArr).not.toEqual(finalClassArr);
  });
});
