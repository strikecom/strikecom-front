import firebase from 'firebase';
import User from '@/store/userhelp';
import { EventFilters, EventRequest, IEvent, PostFavourite } from '@/components/api';
import { $axios } from '@/utils/api';
import qs from 'qs';
import auth = firebase.auth;


export const state = () => ({
  signInModal: false,
  user: null,
  loading: false,
  error: '',
  spinnerOn: true,
  overlayOn: true,
  userBookmarks: [],
  userConflictBookmarks: [],
  userNewsBookmarks: [],
  very: false,
  email: '',
  token: '',
  userImg: null,
  menuOpened: false,
  statisticsCountry: '',
  statisticsPeriod: [null, null],
});

export const mutations = {
  // eslint-disable-next-line no-shadow
  SIGN_IN_MODAL_ON(state: any, value: boolean) {
    // eslint-disable-next-line no-param-reassign
    state.signInModal = value;
  },

  // eslint-disable-next-line no-shadow
  SET_BOOKMARKS(state: any, bookmarkIds: number[]) {
    // eslint-disable-next-line no-param-reassign
    state.userBookmarks = bookmarkIds;
  },

  // eslint-disable-next-line no-shadow
  ADD_BOOKMARK(state: any, id: number) {
    state.userBookmarks.push(id);
  },

  // eslint-disable-next-line no-shadow
  SET_USER_IMG(state: any, payload: any) {
    // eslint-disable-next-line no-param-reassign
    state.userImg = payload;
  },

  // eslint-disable-next-line no-shadow
  DELETE_BOOKMARK(state: any, id: number) {
    const index = state.userBookmarks.indexOf(id);
    if (index > -1) {
      state.userBookmarks.splice(index, 1);
    }
  },

  // eslint-disable-next-line no-shadow
  SET_CONFLICT_BOOKMARKS(state: any, bookmarkIds: number[]) {
    // eslint-disable-next-line no-param-reassign
    state.userConflictBookmarks = bookmarkIds;
  },

  // eslint-disable-next-line no-shadow
  ADD_CONFLICT_BOOKMARK(state: any, id: number) {
    state.userConflictBookmarks.push(id);
  },

  // eslint-disable-next-line no-shadow
  DELETE_CONFLICT_BOOKMARK(state: any, id: number) {
    const index = state.userConflictBookmarks.indexOf(id);
    if (index > -1) {
      state.userConflictBookmarks.splice(index, 1);
    }
  },

  // eslint-disable-next-line no-shadow
  SET_NEWS_BOOKMARKS(state: any, bookmarkIds: number[]) {
    // eslint-disable-next-line no-param-reassign
    state.userNewsBookmarks = bookmarkIds;
  },

  // eslint-disable-next-line no-shadow
  ADD_NEWS_BOOKMARK(state: any, id: number) {
    state.userNewsBookmarks.push(id);
  },

  // eslint-disable-next-line no-shadow
  DELETE_NEWS_BOOKMARK(state: any, id: number) {
    const index = state.userNewsBookmarks.indexOf(id);
    if (index > -1) {
      state.userNewsBookmarks.splice(index, 1);
    }
  },

  // eslint-disable-next-line no-shadow
  SET_SPINNER(state: any, isOn: boolean) {
    // eslint-disable-next-line no-param-reassign
    state.spinnerOn = isOn;
  },

  // eslint-disable-next-line no-shadow
  SET_OVERLAY(state: any, isOn: boolean) {
    // eslint-disable-next-line no-param-reassign
    state.overlayOn = isOn;
  },

  // eslint-disable-next-line no-shadow
  SET_MENU(state: any, isOn: boolean) {
    // eslint-disable-next-line no-param-reassign
    state.menuOpened = isOn;
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  SET_USER(state: any, payload: any) {
    // eslint-disable-next-line no-param-reassign
    state.user = payload;
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  SET_USER_EMAIL(state: any, payload: any) {
    // eslint-disable-next-line no-param-reassign
    state.email = payload;
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  SET_LOADING(state: any, payload: any) {
    // eslint-disable-next-line no-param-reassign
    state.loading = payload;
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  SET_ERROR(state: any, payload: any) {
    // eslint-disable-next-line no-param-reassign
    if (payload === 'The email address is already in use by another account.') {
      // eslint-disable-next-line no-param-reassign
      state.error = 'Данный Email уже используется';
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  SET_TOKEN(state: any, payload: string) {
    // eslint-disable-next-line no-param-reassign
    state.token = payload;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  CLEAR_ERROR(state: any) {
    // eslint-disable-next-line no-param-reassign
    state.error = null;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  SET_STATISTICS_COUNTRY(state: any, payload: string) {
    // eslint-disable-next-line no-param-reassign
    state.statisticsCountry = payload;
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,no-shadow
  SET_STATISTICS_PERIOD(state: any, payload: string[] | null[]) {
    // eslint-disable-next-line no-param-reassign
    state.statisticsPeriod = payload;
  },
};


export const actions = {
  async registerUser({ commit }: any, { email, password } : { email: string, password: string }): Promise<void> {
    commit('CLEAR_ERROR');
    commit('SET_LOADING', true);
    try {
      const user = await firebase.auth().createUserWithEmailAndPassword(email, password);
      const userEmailCheck = user.user;
      if (userEmailCheck) {
        await userEmailCheck.sendEmailVerification();
        window.alert('На вашу почту было отправлено письмо для подтверждения регистрации');
        commit('SET_LOADING', false);
        if (userEmailCheck.emailVerified) {
          if (user.user) {
            commit('SET_USER', new User(user.user.uid));
          }
          commit('SET_LOADING', false);
          commit('SIGN_IN_MODAL_ON', false);
        }
      } else {
        commit('SET_LOADING', false);
      }
    } catch (e) {
      commit('SET_ERROR', e.message);
      commit('SET_LOADING', false);
      throw e;
    }
  },

  async loginUser({ commit }: any, { email, password } : { email: string, password: string }): Promise<void> {
    commit('CLEAR_ERROR');
    commit('SET_LOADING', true);
    try {
      const user = await firebase.auth().signInWithEmailAndPassword(email, password);
      const userEmailCheck = user.user;
      commit('SET_USER_EMAIL', email);
      if (userEmailCheck && userEmailCheck.emailVerified) {
        if (user.user) {
          commit('SET_USER', new User(user.user.uid));
        }
        commit('SET_LOADING', false);
        commit('SIGN_IN_MODAL_ON', false);
      } else {
        commit('SET_LOADING', false);
        window.alert('На вашу почту было отправлено письмо для подтверждения регистрации');
      }
    } catch (e) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', e.message);
      throw e;
    }
  },

  googleAuthorization({ commit }: any): void {
    const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
    auth().signInWithPopup(googleAuthProvider)
      .then((res) => {
        if (res.user) {
          commit('SET_USER', new User(res.user.uid));
          commit('SET_USER_EMAIL', res.user.displayName);
        }
        commit('SET_LOADING', false);
        commit('SIGN_IN_MODAL_ON', false);
      }).catch((e) => {
        window.alert(e.message);
      });
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async resetPassword({ commit }: any, email: string): Promise<void> {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (pattern.test(email)) {
      await firebase.auth().sendPasswordResetEmail(email)
        .then(() => {
          window.alert('Форма восстановления пароля отправлена на вашу почту');
        }).catch(() => {
          window.alert('Данный Email не зарегистрирован');
        });
    } else {
      window.alert('Заполните поле Email');
    }
  },

  async loggedUser ({ commit, dispatch }: any, user: firebase.User): Promise<void> {
    commit('SET_USER', new User(user.uid));
    if (user.displayName != null) {
      commit('SET_USER_EMAIL', user.displayName);
    } else {
      commit('SET_USER_EMAIL', user.email);
    }

    const idToken: string = await user.getIdToken();
    commit('SET_TOKEN', idToken);

    await dispatch('loadBookmarks', 'events');
    await dispatch('loadBookmarks', 'news');
    await dispatch('loadBookmarks', 'conflicts');
  },

  async logoutUser ({ commit, dispatch }: any): Promise<void> {
    await firebase.auth().signOut();
    commit('SET_USER', null);
    commit('SET_TOKEN', '');
    dispatch('clearBookmarks', 'events');
    dispatch('clearBookmarks', 'news');
    dispatch('clearBookmarks', 'conflicts');
  },

  async saveUser({ dispatch, commit }: any): Promise<void> {
    await firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        if ((user.emailVerified === true) || (user.displayName != null)) {
          dispatch('loggedUser', user);
          firebase.storage().ref(`users/${user.uid}/profile.jpg`).getDownloadURL()
            .then((imgUrl) => {
              commit('SET_USER_IMG', imgUrl);
            })
            .catch(() => {
              firebase.storage().ref('images.jpeg').getDownloadURL()
                .then((imgUrl) => {
                  commit('SET_USER_IMG', imgUrl);
                });
            });
        }
      }
    });
  },

  async addBookmark({ commit, getters }: any, { type, id }: {type: string, id: string}): Promise<void> {
    if (!getters.checkUser) {
      window.alert('Пожалуйста, авторизуйтесь перед тем, как добавлять закладки.');
    } else {
      const language = 'ru';
      if (type === 'events') {
        commit('ADD_BOOKMARK', id);
      } else if (type === 'news') {
        commit('ADD_NEWS_BOOKMARK', id);
      } else {
        commit('ADD_CONFLICT_BOOKMARK', id);
      }
      const url: string = `https://zabastcom.org/api/v2/${language}/${type}/${id}/favourites`;
      const favouritePostRequest: PostFavourite = {
        favourite: true,
      };
      let source = $axios.CancelToken.source();
      const config = {
        cancelToken: source.token,
        headers: getters.authorizationToken ? {
          Authorization: `Bearer ${getters.authorizationToken}`,
        } : null,
      };
      // Прерывание запроса через 10 секунд
      setTimeout(() => {
        source.cancel();
        source = $axios.CancelToken.source();
      }, 10000);
      await $axios.$post(url, favouritePostRequest, config);
    }
  },

  async deleteBookmark({ commit, getters }: any, { type, id }: {type: string, id: string}): Promise<void> {
    if (!getters.checkUser) {
      window.alert('Пожалуйста, авторизуйтесь перед тем, как добавлять закладки.');
    } else {
      const language = 'ru';
      if (type === 'events') {
        commit('DELETE_BOOKMARK', id);
      } else if (type === 'news') {
        commit('DELETE_NEWS_BOOKMARK', id);
      } else {
        commit('DELETE_CONFLICT_BOOKMARK', id);
      }
      const url: string = `https://zabastcom.org/api/v2/${language}/${type}/${id}/favourites`;
      const favouritePostRequest: PostFavourite = {
        favourite: false,
      };
      let source = $axios.CancelToken.source();
      const config = {
        cancelToken: source.token,
        headers: getters.authorizationToken ? {
          Authorization: `Bearer ${getters.authorizationToken}`,
        } : null,
      };
      // Прерывание запроса через 10 секунд
      setTimeout(() => {
        source.cancel();
        source = $axios.CancelToken.source();
      }, 10000);
      await $axios.$post(url, favouritePostRequest, config);
    }
  },

  async loadBookmarks({ commit, getters }: any, type: string): Promise<void> {
    const language = 'ru';
    const url: string = `https://zabastcom.org/api/v2/${language}/${type}`;
    let source = $axios.CancelToken.source();
    // Прерывание запроса через 10 секунд
    setTimeout(() => {
      source.cancel();
      source = $axios.CancelToken.source();
    }, 10000);
    const filters: EventFilters = {
      favourites: true,
    };
    const eventRequest: EventRequest = {
      page: '1',
      perPage: '100',
      filters,
    };
    const favouriteEvents: number[] = (await $axios.$get(url, {
      cancelToken: source.token,
      headers: getters.authorizationToken ? {
        Authorization: `Bearer ${getters.authorizationToken}`,
      } : null,
      params: eventRequest,
      paramsSerializer: params => {
        return qs.stringify(params, { allowDots: true });
      },
    })).data.map((item: IEvent) => { return item.id; });
    if (type === 'events') {
      commit('SET_BOOKMARKS', favouriteEvents || []);
    } else if (type === 'news') {
      commit('SET_NEWS_BOOKMARKS', favouriteEvents || []);
    } else {
      commit('SET_CONFLICT_BOOKMARKS', favouriteEvents || []);
    }
  },

  async clearBookmarks({ commit }: any, type: string): Promise<void> {
    if (type === 'events') {
      commit('SET_BOOKMARKS', []);
    } else if (type === 'news') {
      commit('SET_NEWS_BOOKMARKS', []);
    } else {
      commit('SET_CONFLICT_BOOKMARKS', []);
    }
  },
};

export const getters = {
  // eslint-disable-next-line no-shadow
  user(state: any): firebase.User {
    return state.user;
  },
  // eslint-disable-next-line no-shadow
  loading(state: any): boolean {
    return state.loading;
  },
  // eslint-disable-next-line no-shadow
  error(state: any): string {
    return state.error;
  },
  // eslint-disable-next-line no-shadow
  checkUser(state: any): boolean {
    return state.user !== null;
  },
  // eslint-disable-next-line no-shadow
  authorizationToken(state: any): string {
    return state.token;
  },
};
